emacsql (4.1.0-2) UNRELEASED; urgency=medium

  * d/control : drop oldoldstable emacs version from Recommends

 -- Xiyue Deng <manphiz@gmail.com>  Wed, 12 Feb 2025 15:47:02 -0800

emacsql (4.1.0-1) unstable; urgency=medium

  * Update to new upstream version 4.1.0.
  * Drop all patches : the custom connector has been removed by upstream.
  * d/copyright : remove reference to previously removed files.
  * Remove d/elpa-emacsql-sqlite.install : file sqlite/emacsql-sqlite does
    not exist anymore.
  * Remove d/elpa-test : file sqlite/emacsql-sqlite does not exist
    anymore.
  * Remove debian/elpa-emacsql-sqlite.README.Debian : not applicable
    anymore.
  * d/control :
    - remove ${shlibs:Depends} from elpa-emacsql-sqlite dependencies
    - remove libsqlite3-dev from Build-Depends
    - change elpa-emacsql-sqlite arch from any to all (no binaries
      anymore)
  * d/rules : Update DEB_UPSTREAM_VERSION

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Mon, 30 Dec 2024 21:50:22 +0100

emacsql (4.0.3+ds-1) unstable; urgency=medium

  * Update to new upstream version 4.0.3+ds.
  * Add explicit DEB_UPSTREAM_VERSION to d/rules.
  * Bump Standards-Version to 4.7.0 (no changes required).

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sun, 15 Sep 2024 00:14:35 +0200

emacsql (3.1.1+git20230417.6401226+ds-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 20:00:50 +0900

emacsql (3.1.1+git20230417.6401226+ds-1) unstable; urgency=medium

  [ Aymeric Agon-Rambosson ]
  * Update to new upstream version 3.1.1+git20230417.6401226+ds : needed
    to repair magit after release of emacs 1:29.1+1-1 (see
    https://github.com/magit/forge/issues/535) (Closes: #1051090).
  * Bump Standards-Version to 4.6.2 (no changes required).
  * d/control : update Homepage.
  * Specify dependencies manually between backends and high-level
    abstractions (discrepancy in version numbering format made backends
    unupgradeable).
  * Add new upstream files to d/elpa-emacsql-sqlite.elpa.
  * d/rules: add binary hardening options.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 13 Sep 2023 09:28:28 +0100

emacsql (3.1.1+ds-1) unstable; urgency=medium

  * Adopt the package from Sean Whitton.
  * Bump debhelper compat to 13.
  * Bump Standards version to 4.5.1 (no changes required).
  * Add optional Rules-Requires-Root field to d/control to respect policy.
  * Update to new upstream version 3.1.1+ds.
  * Update d/watch to match new upstream repo.
  * Add disambiguation to d/control short descriptions.
  * Update d/copyright to match new upstream repo, correct typo.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Mon, 31 Oct 2022 22:45:34 +0100

emacsql (3.0.0+ds-2) unstable; urgency=medium

  * Source-only upload.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 26 Jul 2020 14:35:56 -0700

emacsql (3.0.0+ds-1) unstable; urgency=medium

  * Initial release (Closes: #962688).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 27 Jun 2020 13:04:14 -0700
